import { Component, OnInit } from '@angular/core';
import { MatDialog} from '@angular/material/dialog'

@Component({
  selector: 'app-entradamer',
  templateUrl: './entradamer.component.html',
  styles: [
  ]
})
export class EntradamerComponent implements OnInit {

  // filtro entrada mercaderia
  filtroEntradaMercaderia:boolean = false;
  // abrir nuevo entradamercaderia
  abrirnuevoentradamercaderia = false;
  // cerrar el modal
  cerrar=false;

  constructor( public modal:MatDialog) { 
  }

  ngOnInit(): void {
  }

  // Filtro de entrada mercaderia
  onMostratrFiltroEntradaMercaderia(){
    if(this.filtroEntradaMercaderia) this.filtroEntradaMercaderia = false;
    else this.filtroEntradaMercaderia = true;
  }
  // Cerrar entrada mercaderia
  CerrarNuevoEntradaMercaderia()
  {
    this.cerrar=true;
    setTimeout(() => {
      this.abrirnuevoentradamercaderia = false;
      this.cerrar = false;
    }, 1000);
  }

}





