import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EliminarComponent } from './modals/eliminar/eliminar.component';
import { NuevoproveedorComponent } from './modals/nuevoproveedor/nuevoproveedor.component';

@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.component.html',
  styles: [
  ]
})

export class ProveedoresComponent implements OnInit {

  filtroProveedores:boolean = false;
  VerFiltroTresPuntos:boolean = false;

  constructor(public modal:MatDialog) { }

  ngOnInit(): void {
  }

  onModalNuevoProveedor(){
    this.modal.open(NuevoproveedorComponent)
  }

  onModalEliminarProveedor(){
    this.modal.open(EliminarComponent)
  }

  MostrarFiltroTresPuntos(){
    if(this.VerFiltroTresPuntos) this.VerFiltroTresPuntos = false;
    else this.VerFiltroTresPuntos = true;
  }

  onMostratrFiltroProveedores(){
    if(this.filtroProveedores) this.filtroProveedores = false;
    else this.filtroProveedores = true;
  }
}
