import { Component, OnInit } from '@angular/core';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';


export interface CorreoE {
  name: string;
}


@Component({
  selector: 'app-nuevoproveedor',
  templateUrl: './nuevoproveedor.component.html',
  styles: [
  ]
})
export class NuevoproveedorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  correo: CorreoE[] = [
    {name: 'emplo1@gmail.com'}
  ];

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.correo.push({name: value});
    }

    // Clear the input value
    // event.chipInput!.clear();
  }

  remove(correoelect: CorreoE): void {
    const index = this.correo.indexOf(correoelect);

    if (index >= 0) {
      this.correo.splice(index, 1);
    }
  }

}
