import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProveedoresComponent } from './proveedores/proveedores.component';
import { DemoMaterialModule } from 'src/app/material-module';
import { NuevoproveedorComponent } from './proveedores/modals/nuevoproveedor/nuevoproveedor.component';
import { EliminarComponent } from './proveedores/modals/eliminar/eliminar.component';
import { EntradamerComponent } from './entradamer/entradamer.component';


@NgModule({
  declarations: [
    ProveedoresComponent, 
     NuevoproveedorComponent,
     EliminarComponent, 
     EntradamerComponent
    
    
  ],

  imports: [
    CommonModule,
    RouterModule,
    AppRoutingModule,
    SharedModule,
    DemoMaterialModule
  ]
})
export class ComprasModule { }
