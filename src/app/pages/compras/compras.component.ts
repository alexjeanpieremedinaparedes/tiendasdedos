import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-compras',
  templateUrl: './compras.component.html',
  styles: [
  ]
})
export class ComprasComponent implements OnInit {

  VerFiltroCajas:boolean = false;
    
  constructor() { }

  ngOnInit(): void {
  }

  MostrarFiltroCajas(){
    if(this.VerFiltroCajas) this.VerFiltroCajas = false;
    else  this.VerFiltroCajas = true;
  }
}

