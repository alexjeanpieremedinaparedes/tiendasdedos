import { Component, OnInit } from '@angular/core';
import { MatDialog} from '@angular/material/dialog'

@Component({
  selector: 'app-entradamercaderia',
  templateUrl: './entradamercaderia.component.html',
  styles: [
  ]
})
export class EntradamercaderiaComponent implements OnInit {

  document:any;
  constructor( public modal:MatDialog) { 
   
  }
  
  VerFiltroEntradaMErcaderia:boolean = false;

  ngOnInit(): void {
  }

  MostrarFiltroEntradaMercaderia(){
    if(this.VerFiltroEntradaMErcaderia) this.VerFiltroEntradaMErcaderia = false;
    else this.VerFiltroEntradaMErcaderia = true;
  }
}
