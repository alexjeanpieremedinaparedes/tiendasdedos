import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog'
import { NuevoalmacenComponent } from './modals/nuevoalmacen/nuevoalmacen.component';

@Component({
  selector: 'app-almacenes',
  templateUrl: './almacenes.component.html',
  styles: [
  ]
})
export class AlmacenesComponent implements OnInit {

    
  VerFiltroAlmacenes:boolean = false;

  
  constructor( public modal:MatDialog) { }

  ngOnInit(): void {
  }
  MostrarFiltroAlmacenes(){
    if(this.VerFiltroAlmacenes) this.VerFiltroAlmacenes = false;
    else this.VerFiltroAlmacenes = true;
  }

  onNuevo(){
    this.modal.open(NuevoalmacenComponent)
  }





  
}
