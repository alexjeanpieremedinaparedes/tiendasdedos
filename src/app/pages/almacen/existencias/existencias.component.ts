import { Component, OnInit } from '@angular/core';
import { MatDialog} from '@angular/material/dialog'
import { DescargaComponent } from './modals/descarga/descarga.component';
@Component({
  selector: 'app-existencias',
  templateUrl: './existencias.component.html',
  styles: [
  ]
})
export class ExistenciasComponent implements OnInit {
  VerFiltroListado:boolean = false;
  VerFiltroMuestra:boolean = false;
  
  VerBotonFiltroListado:boolean = true;
  VerBotonFiltroMuestra:boolean = false;
  VerBotonDescarga:boolean = false;

  constructor( public modal:MatDialog) { 
   
  }
  
  ngOnInit(): void {

  }

  onMostrarBotonFiltroListado(){
    this.VerBotonFiltroListado = true;
    this.VerBotonFiltroMuestra = false;
    this.VerBotonDescarga = false;
  }

  onMostrarBotonMuestra(){
    this.VerBotonFiltroMuestra = true;
    this.VerBotonFiltroListado = false;
    this.VerBotonDescarga = false;
  }

  onMostrarBotonDescarga(){
    this.VerBotonDescarga = true;
    this.VerBotonFiltroListado = false;
    this.VerBotonFiltroMuestra = false;
  }
  
  MostrarFiltroListado(){
    if(this.VerFiltroListado) this.VerFiltroListado = false;
    else this.VerFiltroListado = true;
  }
  MostrarFiltroMuestra(){
    if(this.VerFiltroMuestra) this.VerFiltroMuestra = false;
    else this.VerFiltroMuestra = true;
  }

  MostrarDescarga(){
    this.modal.open(DescargaComponent)
  }

 
}
