import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { AlmacenesComponent } from './almacenes/almacenes.component';
import { ExistenciasComponent } from './existencias/existencias.component';
import { EntradamercaderiaComponent } from './entradamercaderia/entradamercaderia.component';
import { TransferenciasComponent } from './transferencias/transferencias.component';
import { InventarioComponent } from './inventario/inventario.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DemoMaterialModule } from 'src/app/material-module';
import { NuevoalmacenComponent } from './almacenes/modals/nuevoalmacen/nuevoalmacen.component';
import { NuevoinventarioComponent } from './inventario/modals/nuevoinventario/nuevoinventario.component';




@NgModule({
  declarations: [
    AlmacenesComponent,
    ExistenciasComponent,
    EntradamercaderiaComponent,
    TransferenciasComponent,
    InventarioComponent,
    NuevoalmacenComponent,
    NuevoinventarioComponent,
  
  ],
  imports: [
    CommonModule,
    RouterModule,
    AppRoutingModule,
    SharedModule,
    DemoMaterialModule
  ]
})
export class AlmacenModule { }
