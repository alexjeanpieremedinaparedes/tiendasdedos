import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog' 
import { NuevoinventarioComponent } from './modals/nuevoinventario/nuevoinventario.component';

@Component({
  selector: 'app-inventario',
  templateUrl: './inventario.component.html',
  styles: [
  ]
})
export class InventarioComponent implements OnInit {

  VerFiltroInventario:boolean = false;

  constructor( public modal:MatDialog ) { }

  ngOnInit(): void {
  }

  onNuevoInventario(){
    this.modal.open(NuevoinventarioComponent)
  }

  MostrarFiltroInventario(){
   
    if(this.VerFiltroInventario) this.VerFiltroInventario = false;
    else this.VerFiltroInventario = true;
  }
}
