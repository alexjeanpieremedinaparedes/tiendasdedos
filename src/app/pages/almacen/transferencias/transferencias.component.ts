import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-transferencias',
  templateUrl: './transferencias.component.html',
  styles: [
  ]
})
export class TransferenciasComponent implements OnInit {

  VerFiltroEntradaMErcaderia:boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  MostrarFiltroEntradaMercaderia(){
    if(this.VerFiltroEntradaMErcaderia) this.VerFiltroEntradaMErcaderia = false;
    else this.VerFiltroEntradaMErcaderia = true;
  }
}
