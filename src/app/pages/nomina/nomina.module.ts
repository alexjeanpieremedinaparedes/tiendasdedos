import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonaloperativoComponent } from './personaloperativo/personaloperativo.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { RouterModule } from '@angular/router';

import { DemoMaterialModule } from 'src/app/material-module';
import { NuevopersonaloperativoComponent } from './personaloperativo/modals/nuevopersonaloperativo/nuevopersonaloperativo.component';




@NgModule({
  declarations: [
    PersonaloperativoComponent,
    NuevopersonaloperativoComponent,

  ],
  imports: [
    CommonModule,
    RouterModule,
    AppRoutingModule,
    SharedModule,
    DemoMaterialModule
  ]
})
export class NominaModule { }
