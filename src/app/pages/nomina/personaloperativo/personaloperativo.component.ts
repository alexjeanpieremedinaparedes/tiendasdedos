import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog'
import { NuevopersonaloperativoComponent } from './modals/nuevopersonaloperativo/nuevopersonaloperativo.component';

@Component({
  selector: 'app-personaloperativo',
  templateUrl: './personaloperativo.component.html',
  styles: [
  ]
})
export class PersonaloperativoComponent implements OnInit {
  
  VerFiltroNomina:boolean = false;
  VerFiltroTresPuntos:boolean = false;
  VerFiltroEntradaMErcaderia:boolean = false;


  constructor(public modal:MatDialog) { }

  ngOnInit(): void {
  }

  MostrarFiltroNomina(){
    if(this.VerFiltroNomina) this.VerFiltroNomina = false;
    else this.VerFiltroNomina = true;
  }

  MostrarFiltroTresPuntos(){
    if(this.VerFiltroTresPuntos) this.VerFiltroTresPuntos = false;
    else this.VerFiltroTresPuntos = true;
  }

  onNuevoPersonalOPerativo(){
    this.modal.open(NuevopersonaloperativoComponent)
  }

  MostrarFiltroEntradaMercaderia(){
    if(this.VerFiltroEntradaMErcaderia) this.VerFiltroEntradaMErcaderia = false;
    else this.VerFiltroEntradaMErcaderia = true;
  }
}
