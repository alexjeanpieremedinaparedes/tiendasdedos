import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog'

@Component({
  selector: 'app-cuentas',
  templateUrl: './cuentas.component.html',
  styles: [
  ]
})
export class CuentasComponent implements OnInit {

  VerFiltroNomina:boolean = false;
  VerFiltroTresPuntos:boolean = false;

  VerFiltroEntradaMErcaderia:boolean = false;


  constructor(public modal:MatDialog) { }

  ngOnInit(): void {
  }

  MostrarFiltroNomina(){
    if(this.VerFiltroNomina) this.VerFiltroNomina = false;
    else this.VerFiltroNomina = true;
  }

  MostrarFiltroTresPuntos(){
    if(this.VerFiltroTresPuntos) this.VerFiltroTresPuntos = false;
    else this.VerFiltroTresPuntos = true;
  }

 

  MostrarFiltroEntradaMercaderia(){
    if(this.VerFiltroEntradaMErcaderia) this.VerFiltroEntradaMErcaderia = false;
    else this.VerFiltroEntradaMErcaderia = true;
  }
  
}
