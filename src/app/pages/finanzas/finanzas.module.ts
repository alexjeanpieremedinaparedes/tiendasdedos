import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CuentasComponent } from './cuentas/cuentas.component';
import { CajasComponent } from './cajas/cajas.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    CuentasComponent,
    CajasComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    AppRoutingModule,
    SharedModule
  ]
})
export class FinanzasModule { }
