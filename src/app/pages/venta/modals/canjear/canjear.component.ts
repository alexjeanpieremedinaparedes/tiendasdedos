import { Component, OnInit } from '@angular/core';
import {MatDialog } from '@angular/material/dialog'
import { NotacreditoComponent } from '../notacredito/notacredito.component';

@Component({
  selector: 'app-canjear',
  templateUrl: './canjear.component.html',
  styles: [
  ]
})
export class CanjearComponent implements OnInit {
  
  VerFiltroTresPuntos:boolean = false;
  constructor( public modal:MatDialog ) { }

  ngOnInit(): void {
  }

  MostrarFiltroTresPuntos(){
    if(this.VerFiltroTresPuntos) this.VerFiltroTresPuntos = false;
    else this.VerFiltroTresPuntos = true;
  }

  onNotaCredito(){
    this.modal.closeAll(),
    this.modal.open(NotacreditoComponent)
    
  }

}
