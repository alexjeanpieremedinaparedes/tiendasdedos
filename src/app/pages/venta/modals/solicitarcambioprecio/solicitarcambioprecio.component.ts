import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog'
import { RespuestacambioprecioComponent } from '../respuestacambioprecio/respuestacambioprecio.component';

@Component({
  selector: 'app-solicitarcambioprecio',
  templateUrl: './solicitarcambioprecio.component.html',
  styles: [
  ]
})
export class SolicitarcambioprecioComponent implements OnInit {

  constructor( public modal :MatDialog ) { }

  ngOnInit(): void {
  }

  onRespuestaCambioPrecio(){
    
    this.modal.closeAll(),
    this.modal.open(RespuestacambioprecioComponent)
    // setTimeout(() => {
    //   this.modal.closeAll();
    // }, 3500);
  }

  
}
