import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog'
import { OkComponent } from '../ok/ok.component';

@Component({
  selector: 'app-notacredito',
  templateUrl: './notacredito.component.html',
  styles: [
  ]
})
export class NotacreditoComponent implements OnInit {

  constructor(public modal:MatDialog) { }

  ngOnInit(): void {
  }

  onOk(){
    this.modal.closeAll(),
    this.modal.open(OkComponent)
    setTimeout(() => {
      this.modal.closeAll();
    }, 3500);

   
  }

}
