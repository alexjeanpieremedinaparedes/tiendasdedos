import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog'
import { RespuestapermisoComponent } from '../respuestapermiso/respuestapermiso.component';

@Component({
  selector: 'app-anular',
  templateUrl: './anular.component.html',
  styles: [
  ]
})
export class AnularComponent implements OnInit {

  VerFiltroTresPuntos:boolean = false;
  constructor(public modal:MatDialog) { }

  ngOnInit(): void {
  }

  MostrarFiltroTresPuntos(){
    if(this.VerFiltroTresPuntos) this.VerFiltroTresPuntos = false;
    else this.VerFiltroTresPuntos = true;
  }

  onContinuar(){
    this.modal.closeAll()
    this.modal.open(RespuestapermisoComponent)
  }

}
