import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { BolsavaciaComponent } from './modals/bolsavacia/bolsavacia.component';
import { CanjearComponent } from './modals/canjear/canjear.component';
import { AnularComponent } from './modals/anular/anular.component';
import { PermisoComponent } from './modals/permiso/permiso.component';
import { SolicitarcambioprecioComponent } from './modals/solicitarcambioprecio/solicitarcambioprecio.component'
@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls:['./venta.style.css'],
})
export class VentaComponent implements OnInit {
  
  document:any;
  continuar:boolean = false;
  tarjeta:boolean;

  constructor( public modal:MatDialog) { 
    this.tarjeta = false;
  }

  tarjetaindividualvisa: boolean = false;
  tarjetaindividualmastercard: boolean = false;
  tarjetaindividualamercian: boolean = false;
  tarjetaindividualdiners: boolean = false;
  
  OnModalPagarTarjetaVisa(){
    this.tarjetaindividualvisa = !this.tarjetaindividualvisa;
    this.tarjetaindividualmastercard = false;
    this.tarjetaindividualamercian = false;
    this.tarjetaindividualdiners = false;
  }

  OnModalPagarTarjetaMastercard(){
    this.tarjetaindividualmastercard = !this.tarjetaindividualmastercard;
    this.tarjetaindividualvisa = false;
    this.tarjetaindividualamercian = false;
    this.tarjetaindividualdiners = false;
  }

  OnModalPagarTarjetaAmerican(){
    this.tarjetaindividualamercian = !this.tarjetaindividualamercian;
    this.tarjetaindividualvisa = false;
    this.tarjetaindividualmastercard = false;
    this.tarjetaindividualdiners = false;
  }

  OnModalPagarTarjetaDiners(){
    this.tarjetaindividualdiners = !this.tarjetaindividualdiners;
    this.tarjetaindividualvisa = false;
    this.tarjetaindividualmastercard = false;
    this.tarjetaindividualamercian = false;
  }

  onCanjear(){
    this.modal.open(CanjearComponent)
  }

  onAnular(){
    this.modal.open(AnularComponent)
  }

  onContinuar(){
    this.continuar = true;
  }

  // onPermiso(){
  //   this.modal.open(PermisoComponent)
  // }

  onPermiso(){
    this.modal.open(PermisoComponent)
    setTimeout(() => {
      this.modal.closeAll();
    }, 3500);
  }

  onSolicarCambioPrecio(){
    this.modal.open(SolicitarcambioprecioComponent)
  }

  abrirvender = false;
  abrircomprobante = false;

  cerrar=false;
  // cerrarcomprobante = false;
  opciones: boolean  = true;
  desplegable: boolean = false;
  // eventos para seleccionar de forma global a los checkbox
  nino: boolean  = false;
  nina: boolean  = false;
  VerFiltro:boolean= false;
  VerComprobante:boolean= false;
 
  // pasos
  modalspasos = 1;
  toggleTabs($tabNumber: number){
    this.modalspasos = $tabNumber;
  }
  
  ngOnInit(): void {

  }

  // --------------------------------------------- Componentes de ejecucion (COMBO COX PADRES)---------------------------------------------------------------
 
  onTarjeta(){
    this.tarjeta = true; 
  }

  
  // Checked padre Niño
  onNino(event:any)
  {
    this.nino = event.target.checked;
 
  }
// Checked padre Niña
  onNina(event:any)
  {
    this.nina = event.target.checked;
  }

  
  Cerrar()
  {
    this.cerrar=true;
    setTimeout(() => {
      this.abrirvender = false;
      this.cerrar = false;
    }, 1000);
  }

  CerrarComprobante()
  {
    this.cerrar=true;
    setTimeout(() => {
      this.abrircomprobante = false;
      this.cerrar = false;
    }, 1000);
  }


  // --------------------------------------------- Componentes de ejecucion ( APARECER Y DESAPARECER COMPONENTES DROWPDOWN )---------------------------------------------------------------
  // Mostrar Filtro en la pagina Punto de venta del tab Listado
  MostrarFiltro(){
    if(this.VerFiltro) this.VerFiltro = false;
    else this.VerFiltro = true;
  }

  MostrarComprobante(){
    if(this.VerComprobante) this.VerComprobante = false;
    else this.VerComprobante = true;
  }

  // OcultaElementos y Muestra Elementos
  MuestraListadoPedido(){this.desplegable = true}
  OcultaListadoPedido(){this.desplegable = false}

  MostrarOpcionesTabListado(){ this.opciones = true }
  OcultarOpcionesTabListado(){ this.opciones = false }

  // --------------------------------------------------------------------------PORVERSE
  // Abre Modal de Derecha a Izquierda
  openNav() {
    this.document.getElementById("Medium").style.width = "100%";
    this.document.getElementById("Medium").style.left = "0%";
  
  }
  // Cierra Modal de Izqueirda a Derecha
  closeNav() {
    this.document.getElementById("Medium").style.width = "30%";
    this.document.getElementById("Medium").style.left = "100%";
  }
  
  offVacio(){
    this.modal.open(BolsavaciaComponent)
  }

  checked: boolean = true;
  displayedColumns = ['select', 'tienda', 'producto', 'genero', 'talla', 'stock', 'precio', 'tipo'];
  dataSource = new MatTableDataSource<Element>(ELEMENT_DATA);
  selection = new SelectionModel<Element>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
    this.selection.clear() :
    this.dataSource.data.forEach(row => this.selection.select(row));
  }
}
export interface Element {
  tienda: string;
  producto: string;
  genero: string;
  talla: string;
  stock: number;
  precio: string;
  tipo: string;
}

const ELEMENT_DATA: Element[] = [
  {tienda: 'Tienda-1', producto: 'Hydrogen', genero: 'hombre', talla: '7x', stock:2 , precio: 'S/ 125.00', tipo:'normal'},
  {tienda: 'Tienda-1', producto: 'Helium', genero: 'hombre', talla: '8', stock:2 , precio: 'S/ 155.00', tipo:'normal'},
  {tienda: 'Tienda-1', producto: 'Lithium', genero: 'hombre', talla: '8', stock:2 , precio: 'S/ 225.00', tipo:'ahuesado'},
  {tienda: 'Tienda-1', producto: 'Beryllium', genero: 'hombre', talla: '8x', stock:2 , precio: 'S/ 325.00', tipo:'ahuesado'},
  {tienda: 'Tienda-1', producto: 'Boron', genero: 'hombre', talla: '8', stock:2 , precio: 'S/ 225.00', tipo:'ahuesado'},
  {tienda: 'Tienda-1', producto: 'Carbon', genero: 'hombre', talla: '7', stock:2 , precio: 'S/ 235.00', tipo:'normal'},
  {tienda: 'Tienda-1', producto: 'Nitrogen', genero: 'hombre', talla: '5', stock:2 , precio: 'S/ 525.00', tipo:'ahuesado'},
  {tienda: 'Tienda-1', producto: 'Oxygen', genero: 'hombre', talla: '3', stock:2 , precio: 'S/ 725.00', tipo:'ahuesado'},
  {tienda: 'Tienda-1', producto: 'Fluorine', genero: 'hombre', talla: '2', stock:2 , precio: 'S/ 325.00', tipo:'normal'},
  {tienda: 'Tienda-1', producto: 'Neon', genero: 'hombre', talla: '8', stock:2 , precio: 'S/ 125.00', tipo:'ahuesado'},
  {tienda: 'Tienda-1', producto: 'Sodium', genero: 'hombre', talla: '4', stock:2 , precio: 'S/ 525.00', tipo:'normal'},
  {tienda: 'Tienda-1', producto: 'Magnesium', genero: 'hombre', talla: '6px', stock:2 , precio: 'S/ 125.00', tipo:'ahuesado'},
  {tienda: 'Tienda-1', producto: 'Aluminum',genero: 'hombre', talla: '5px', stock:2 , precio: 'S/ 225.00', tipo:'normal'},
  {tienda: 'Tienda-1', producto: 'Silicon', genero: 'hombre', talla: '7px', stock:2 , precio: 'S/ 325.00', tipo:'ahuesado'},
  {tienda: 'Tienda-1', producto: 'Phosphorus', genero: 'hombre', talla: '9px', stock:2 , precio: 'S/ 425.00', tipo:'normal'},
  {tienda: 'Tienda-1', producto: 'Sulfur', genero: 'hombre', talla: '2', stock:2 , precio: 'S/ 725.00', tipo:'ahuesado'},
  {tienda: 'Tienda-1', producto: 'Chlorine', genero: 'hombre', talla: '7', stock:2 , precio: 'S/ 225.00', tipo:'ahuesado'},
  {tienda: 'Tienda-1', producto: 'Argon', genero: 'hombre', talla: '6', stock:2 , precio: 'S/ 125.00', tipo:'normal'},
  {tienda: 'Tienda-1', producto: 'Potassium', genero: 'hombre', talla: '8', stock:2 , precio: 'S/ 625.00', tipo:'ahuesado'},
  {tienda: 'Tienda-1', producto: 'Calcium', genero:'hombre', talla: '2', stock:2 , precio: 'S/ 425.00', tipo:'ahuesado'},
];

