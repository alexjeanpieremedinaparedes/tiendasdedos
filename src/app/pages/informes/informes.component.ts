import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-informes',
  templateUrl: './informes.component.html',
  styles: [
  ]
})
export class InformesComponent implements OnInit {

  VerFiltroCajas:boolean = false;
    
  constructor() { }

  ngOnInit(): void {
  }

  MostrarFiltroCajas(){
    if(this.VerFiltroCajas) this.VerFiltroCajas = false;
    else  this.VerFiltroCajas = true;
  }
}
