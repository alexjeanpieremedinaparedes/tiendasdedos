import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlmacenComponent } from './almacen/almacen.component';
import { AlmacenesComponent } from './almacen/almacenes/almacenes.component';
import { EntradamercaderiaComponent } from './almacen/entradamercaderia/entradamercaderia.component';
import { ExistenciasComponent } from './almacen/existencias/existencias.component';
import { InventarioComponent } from './almacen/inventario/inventario.component';
import { TransferenciasComponent } from './almacen/transferencias/transferencias.component';
import { ComprasComponent } from './compras/compras.component';
import { EntradamerComponent } from './compras/entradamer/entradamer.component';
import { ProveedoresComponent } from './compras/proveedores/proveedores.component';
import { CajasComponent } from './finanzas/cajas/cajas.component';
import { CuentasComponent } from './finanzas/cuentas/cuentas.component';
import { FinanzasComponent } from './finanzas/finanzas.component';
import { GestionpedidoComponent } from './gestionpedido/gestionpedido.component';
import { InformesComponent } from './informes/informes.component';
import { InicioComponent } from './inicio.component';
import { NominaComponent } from './nomina/nomina.component';
import { PersonaloperativoComponent } from './nomina/personaloperativo/personaloperativo.component';
import { VentaComponent } from './venta/venta.component';

const routes: Routes = [
  
    {path:'inicio',component:InicioComponent,
    
      children:[
        {path:'venta',component:VentaComponent},
        {path:'compras',component:ComprasComponent,
      
          children:[
              {path:'provedores',component:ProveedoresComponent},
              {path:'entradamer',component:EntradamerComponent},
              {path:'**',redirectTo:'proveedores'}
          ]

        },
        
        {path:'almacen',component:AlmacenComponent,
          
          children:[
            {path:'almacenes',component:AlmacenesComponent},
            {path:'entradamercaderia',component:EntradamercaderiaComponent},
            {path:'existencias',component:ExistenciasComponent},
            {path:'inventario',component:InventarioComponent},
            {path:'transferencias',component:TransferenciasComponent},
            {path:'**',redirectTo:'almacenes'}
          ]
        
        },

        {path:'gestionpedido',component:GestionpedidoComponent},

        {path:'finanzas',component:FinanzasComponent,
      
          children:[
            {path:'cajas',component:CajasComponent},
            {path:'cuentas',component:CuentasComponent},        
            {path:'**',redirectTo:'cajas'}
          ]
 
        },

        {path:'nomina',component:NominaComponent,
        
          children:[
            {path:'personaloperativo',component:PersonaloperativoComponent},         
            {path:'**',redirectTo:'personaloperativo'}
          ]
        },
        {path:'informes',component:InformesComponent},
        {path:'**',redirectTo:'venta'}
      ]
    }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
