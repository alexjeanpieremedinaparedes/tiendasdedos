import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog'


@Component({
  selector: 'app-gestionpedido',
  templateUrl: './gestionpedido.component.html',
  styles: [
  ]
})
export class GestionpedidoComponent implements OnInit {
  
  VerFiltroListado:boolean = false;
  VerFiltroMuestra:boolean = false;

  VerTiempoEntrega:boolean = false;
  VerFiltroSeguimiento:boolean = false;
  
  VerOpcionBotonListadoPedido:boolean = true;
  VerOpcionBotonSeguimiento:boolean = false;
  VerOpcionBotonRetornos:boolean = false;


  desplegable: boolean = false;


  constructor( public modal:MatDialog) { 
  }
  
  ngOnInit(): void {
  }

  onMostrarOpcionBotonListadoPedido(){
    this.VerOpcionBotonListadoPedido = true;
    this.VerOpcionBotonSeguimiento = false;
    this.VerOpcionBotonRetornos = false;
  }

  onMostrarOpcionBotonSeguimiento(){
    this.VerOpcionBotonSeguimiento = true;
    this.VerOpcionBotonListadoPedido = false;
    this.VerOpcionBotonRetornos = false;
  }

  onMostrarOpcionBotonRetornos(){
    this.VerOpcionBotonRetornos= true;
    this.VerOpcionBotonSeguimiento = false;
    this.VerOpcionBotonListadoPedido = false;
  }

  MostrarTiempoEntrega(){
    if(this.VerTiempoEntrega) this.VerTiempoEntrega= false;
    else this.VerTiempoEntrega = true;
  }

  MostrarFiltroSeguimiento(){
    if(this.VerFiltroSeguimiento) this.VerFiltroSeguimiento= false;
    else this.VerFiltroSeguimiento = true; 
  }

    // OcultaElementos y Muestra Elementos
    MuestraListadoPedido(){this.desplegable = true}
    OcultaListadoPedido(){this.desplegable = false}
  
  
   
  
}
