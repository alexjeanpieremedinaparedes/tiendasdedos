import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { VentaComponent } from './venta/venta.component';
import { ComprasComponent } from './compras/compras.component';
import { AlmacenComponent } from './almacen/almacen.component';
import { GestionpedidoComponent } from './gestionpedido/gestionpedido.component';
import { FinanzasComponent } from './finanzas/finanzas.component';
import { NominaComponent } from './nomina/nomina.component';
import { InformesComponent } from './informes/informes.component';
import { InicioComponent } from './inicio.component';
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from '../app-routing.module';
import { BolsavaciaComponent } from './venta/modals/bolsavacia/bolsavacia.component';
import { DemoMaterialModule } from '../material-module';
import { AlmacenModule } from './almacen/almacen.module';
import { CajasComponent } from './finanzas/cajas/cajas.component';
import { CuentasComponent } from './finanzas/cuentas/cuentas.component';

import { RouterModule } from '@angular/router';
import { NominaModule } from './nomina/nomina.module';
import { ComprasModule } from './compras/compras.module';
import { CanjearComponent } from './venta/modals/canjear/canjear.component';
import { AnularComponent } from './venta/modals/anular/anular.component';
import { PermisoComponent } from './venta/modals/permiso/permiso.component';
import { NotacreditoComponent } from './venta/modals/notacredito/notacredito.component';
import { OkComponent } from './venta/modals/ok/ok.component';
import { SolicitarcambioprecioComponent } from './venta/modals/solicitarcambioprecio/solicitarcambioprecio.component';
import { RespuestacambioprecioComponent } from './venta/modals/respuestacambioprecio/respuestacambioprecio.component';
import { RespuestapermisoComponent } from './venta/modals/respuestapermiso/respuestapermiso.component';




@NgModule({
  declarations: [
         VentaComponent,
         ComprasComponent,
         AlmacenComponent,
         GestionpedidoComponent,
         FinanzasComponent,
         NominaComponent,
         InformesComponent,
         InicioComponent,
         BolsavaciaComponent,
         CajasComponent,
         CuentasComponent,
         CanjearComponent,
         AnularComponent,
         PermisoComponent,
         NotacreditoComponent,
         OkComponent,
         SolicitarcambioprecioComponent,
         RespuestacambioprecioComponent,
         RespuestapermisoComponent   
  ],
  exports:[
    VentaComponent,
    ComprasComponent,
    AlmacenComponent,
    GestionpedidoComponent,
    FinanzasComponent,
    NominaComponent,
    InformesComponent
  ],

  imports: [
    CommonModule,
    PagesRoutingModule,
    PagesRoutingModule,
    SharedModule,
    AppRoutingModule, 
    DemoMaterialModule,
    AlmacenModule,
    RouterModule,
    NominaModule, 
    ComprasModule
    
  ],
  
})
export class PagesModule { }
