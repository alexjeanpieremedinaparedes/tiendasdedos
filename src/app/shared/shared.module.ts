import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarprincipalComponent } from './navbarprincipal/navbarprincipal.component';
import { FooterComponent } from './footer/footer.component';
import { AppRoutingModule } from '../app-routing.module';



@NgModule({
  // Lo componentes Declaros dentro del modulo shared
  declarations: [
    // Componentes
    NavbarComponent,
    SidebarComponent,
    NavbarprincipalComponent,
    FooterComponent,
  
  ],

  imports: [
    CommonModule,
    AppRoutingModule
  ],

  exports:[
    NavbarComponent,
    SidebarComponent,
    NavbarprincipalComponent,
    FooterComponent
  ]

  

})
export class SharedModule { }
