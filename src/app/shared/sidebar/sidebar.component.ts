import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.styles.css']
})
export class SidebarComponent implements OnInit {

  estadisticas:boolean = true;
  
  constructor() { }

  ngOnInit(): void {
  }

  MostrarEstadistica(){
    this.estadisticas = true;
  }

  OcultarEstadistica(){
    this.estadisticas = false;
  }
}
