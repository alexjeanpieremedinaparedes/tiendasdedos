const { guessProductionMode } = require("@ngneat/tailwind");

process.env.TAILWIND_MODE = guessProductionMode() ? 'build' : 'watch';

module.exports = {
 
    prefix: '',
    mode: 'jit',
    purge: {
      content: [
        './src/**/*.{html,ts,css,scss,sass,less,styl}',
      ]
    },
    darkMode: 'class', // or 'media' or 'class'
    theme: {

      fontSize: {
        'xs': '.75rem',
        'ven': '.840rem',
        'sm': '.875rem',
        'tiny': '.875rem',
        'am':  '0.932rem',
        'base': '1rem',
        'lg': '1.125rem',
        'xl': '1.25rem',
        '2xl': '1.5rem',
        '3xl': '1.875rem',
        '4xl': '2.25rem',
        '5xl': '3rem',
        '6xl': '4rem',
        '7xl': '5rem',
       },


      borderWidth: {
        default: '1px',
        '0': '0',
        '1': '1px',
        '1.5':'0.11em',
        '2': '2px',
        '3': '3px',
        '4': '4px',
        '6': '6px',
        '8': '8px',
      },


      extend: {
        
        screens: {
          '3xl': '1920px',
        },

        fontWeight: {
          hairline: 100,
          'extra-light': 100,
          thin: 200,
          light: 300,
          normal: 400,
          medium: 500,
          grande: 580,
          semibold: 600,
          masgrande: 605,
          bold: 700,
          extrabold: 800,
          'extra-bold': 800,
          black: 900,
        },


        height: {
            ce:  '500px',
            mp:  '600px',
            sm:  '8px',
            md:  '16px',
            lg:  '24px',
            xl:  '48px',
          '17':  '7rem',
          '62':  '15.2rem',
          '82':  '29rem',
          '83':  '23rem',
          '84':  '35.5rem',
          '85':  '38rem',
          '88':  '40.5rem',
          '90':  '42rem',
          '95':  '50rem',
          '100': '60rem'
         },

        width: {
          '1/7': '14.2857143%',
          '2/7': '28.5714286%',
          '3/7': '42.8571429%',
          '4/7': '57.1428571%',
          '5/7': '71.4285714%',
          '6/7': '85.7142857%',
          '33':  '11rem',
          '40':  '17.5rem',
          '83':  '22rem',
          '97':  '21rem',
          '98':  '29rem',
          '99':  '34.875rem',
          '99.4':'40.625',
          '100': '41.813rem',
          '110': '42rem',
          '120': '45rem',
          '130': '48rem',
          '140': '50rem'
        },

        colors: {

          'morado':{
            '700':'#AE4DDB'
          },

          'amarillo':{
            '700': '#F9C510'
          },

          'rojo':{
            '500': '#EA3E3E',
          },

          'celeste':{
            '20': "#FAFDFF",
            '30': "#59D5FC"
          },

          'azul': {
              '20':  '#EFF4FA',
              '30':  '#EFF6FF',
              '50':  '#F0F6FE',
              '70':  '#DDF1FF',
              '100': '#F7F9FD',
              '150': '#748088',
              '200': '#A9DBF6',
              '230': '#E4EAF3',
              '240': '#C9D7E4',
              '250':' #81A2EB',
              '300': '#1491ED',
              '350': '#148EE0',
              '380': '#B2C0D0',
              '400': '#0067C2',
              '470': '#1491ED', 
              '700': '#606F89',
              '800': '#2B3B43',
              '900': '#191918'                    
          },

          'verde': {
            '100': '#E2FBED',
            '500': '#48C397', 
            '700': '#73C235'            
          },

          'oscuro':{
            '700': '#3A3A3A',
            '750': '#252525',
            '800': '#2D2D2D',
            '810':  '#252424',
            '830': '#0C0C0C',
            '860': '#181818',
            
          },

        
          'plomo': {
              '20':  '#FFFFFF',
              '50':  '#e7e2e2',
              '70':  '#CACACA',
              '100': '#F0F0F0',
              '120': '#ADADAD',
              '123': '#C9C9C9',
              '124': '#D1D1D1',
              '125': '#D8D8D8',
              '128': '#FAFAFA',
              '130': '#A0A6B4',
              '133':  '#E0E0E0',
              '135': '#C4C4C4',
              '137': '#A0A0A0',
              '135': '#FAFAFA',
              '140': '#FAFAFA',
              '145': '#7A8699',                      
              '150': '#F5F3F1',
              '170': '#4F4E4E',
              '180': '#ECECEC',
              '190': '#7D7D7D',
              '200': '#F6F6F6',
              '205': '#DADADA',
              '208': '#D7D7D7',
              '210': '#EBEBEB',
              '215': '#777777',
              '220': '#9C9C9C',
              '225': '#D6D6D6',
              '230': '#8A8A8A',
              '235': '#8C8C8C',
              '240': '#85949E',
              '250': '#C1C6CA',
              '260': '#737373',
              '270': '#5D5D5D',
              '275': '#9A9A9A',
              '280': '#616161',
              '285': '#818181',
              '290': '#424242',
              '295': '#4F4E4E',
              '300': '#BCBCBC',
              '305': '#C5C5C5',
              '310': '#E9E9E9',
              '315': '#DFDFDF',
              '320': '#646464',
              '325': '#424242',
              '327': '#838383',
              '330': '#959595',
              '335': '#B8B8B8',
              '345': '#C7C7C7',
              '350': '#8E8E8E',
              '370': '#4A4A4A',
              '380': '#748088',
              '400': '#707070',
              '420': '#626262',
              '430': '#717070',
              '435': '#616161',            
              '440': '#535353',
              '443': '#5A5A5A',
              '445': '#686868',
              '450': '#7A7A7A',
              '455': '#333333',
              '460': '#9F9F9F',
              '465': '#282828',
              '470': '#616161',
              '480': '#BEBEBE',
              '485': '#7D858E',
              '490': '#5B5B5B',
              '500': '#3E3E3E',
              '550': '#5F5F5F',
              '570': '#888888',
              '600': '#696969',
              '800': '#323232',
              '850': '#383838',
              '900': '#262626'    
          },

          'rojo':{
            '500': '#ED0000',
            '550': '#E03333',
            '700': '#EF1763'
          },

        

          'naranja':{
            '100': '#FF9345',
            '300': '#FFE7D6',
            '500': '#F38C52',
            '600': '#E99649',
            '700': '#F28231'
          }
        },
      
        fontFamily: {
          Roboto: ['Roboto'],
          Poppins: ['Poppins'],
          Manrope: ['Manrope']
        }
      },
    },
    variants: {
      extend: {},
    },
    plugins: [require('@tailwindcss/aspect-ratio'),require('@tailwindcss/forms'),require('@tailwindcss/line-clamp'),require('@tailwindcss/typography')],
};

